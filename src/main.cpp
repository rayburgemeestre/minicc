#include "prelude.hpp"

#ifndef NO_AUDIO
#include <SFML/Audio.hpp>
class sound_base
{
private:
    std::map<std::string, sf::SoundBuffer> sounds_;
    sf::Sound sound[10];
    size_t sound_index;
public:
    sound_base();

    void play(std::string sound);
};

sound_base::sound_base()
    : sound_index(0)
{
    sounds_["Alarm ingeschakeld"].loadFromFile("Alarm ingeschakeld.wav");
    sounds_["Alarm uitgeschakeld"].loadFromFile("Alarm uitgeschakeld.wav");
    sounds_["alarm"].loadFromFile("alarm.wav");
    sounds_["Deur geopend"].loadFromFile("Deur geopend.wav");
    sounds_["Deur gesloten"].loadFromFile("Deur gesloten.wav");
    sounds_["Motion detected"].loadFromFile("Motion detected.wav");
    sounds_["Welkom Anne-Marie"].loadFromFile("Welkom Anne-Marie.wav");
    sounds_["Welkom Gene"].loadFromFile("Welkom Gene.wav");
    sounds_["Welkom Kim"].loadFromFile("Welkom Kim.wav");
    sounds_["Welkom Ray"].loadFromFile("Welkom Ray.wav");
}

void sound_base::play(std::string sound_name)
{
    sound[sound_index].setBuffer(sounds_[sound_name]);
    sound[sound_index].play();
    sound_index++;
    sound_index = sound_index % 10;
}
#endif

using ping = atom_constant<atom("ping")>;
using start = atom_constant<atom("start")>;
using stop = atom_constant<atom("stop")>;
using message_ = atom_constant<atom("message")>;

struct data
{
    caf::actor server;
    // caf::actor example_logic;
};

struct server_data
{
};

behavior server(stateful_actor<server_data> *self);
behavior connected(stateful_actor<data> *self, caf::actor logic);
behavior connected_2(stateful_actor<data> *self, caf::actor logic); // app level connected
behavior connecting(stateful_actor<data> *self, caf::actor logic);

#ifndef NO_AUDIO
struct sound_player_data
{
    caf::actor bridge;
    sound_base soundbase;
};

behavior sound_player(stateful_actor<sound_player_data> *self, bool play_sound, bool sendmsg)
{
    return {
        [=](start, caf::actor sender){
            self->state.bridge = sender;
            aout(self) << "Sound Logic started." << std::endl;
            // self->delayed_send(self, std::chrono::seconds(5), message_::value, "example logic on start event**");
        },
        [=](stop) {
            self->state.bridge = caf::actor();
            aout(self) << "Sound Logic stopped." << std::endl;
            // self->delayed_send(self, std::chrono::seconds(5), message_::value, "example logic on stop event**");
        },
        [=](message_, std::string msg){
            self->send(self->state.bridge, message_::value, msg);
        },
        [=](std::string broadcast_msg) {
            if (play_sound) {
                if (broadcast_msg == "BROADCAST: Alarm ingeschakeld") {
                    self->state.soundbase.play("Alarm ingeschakeld");
                }
                if (broadcast_msg == "BROADCAST: Alarm uitgeschakeld") {
                    self->state.soundbase.play("Alarm uitgeschakeld");
                }
                if (broadcast_msg == "BROADCAST: alarm") {
                    self->state.soundbase.play("alarm");
                }
                if (broadcast_msg == "BROADCAST: Deur geopend") {
                    self->state.soundbase.play("Deur geopend");
                }
                if (broadcast_msg == "BROADCAST: Deur gesloten") {
                    self->state.soundbase.play("Deur gesloten");
                }
                if (broadcast_msg == "BROADCAST: Motion detected") {
                    self->state.soundbase.play("Motion detected");
                }
                if (broadcast_msg == "BROADCAST: Welkom Anne-Marie") {
                    self->state.soundbase.play("Welkom Anne-Marie");
                }
                if (broadcast_msg == "BROADCAST: Welkom Gene") {
                    self->state.soundbase.play("Welkom Gene");
                }
                if (broadcast_msg == "BROADCAST: Welkom Kim") {
                    self->state.soundbase.play("Welkom Kim");
                }
                if (broadcast_msg == "BROADCAST: Welkom Ray") {
                    self->state.soundbase.play("Welkom Ray");
                }
            }
        }
    };
}
#endif

struct console_writer_data
{
    caf::actor bridge;
};

behavior console_writer(stateful_actor<console_writer_data> *self)
{
    return {
        [=](start, caf::actor sender){
            aout(self) << "Console Logic started." << std::endl;
            self->state.bridge = sender;
        },
        [=](stop) {
            aout(self) << "Console Logic stopped." << std::endl;
            self->state.bridge = caf::actor();
        },
        [=](std::string broadcast_msg) {
            aout(self) << "***!*** " << broadcast_msg << "***!***" << std::endl;
        }
    };
}

bool broadcast = false; // HACK
behavior connected_2(stateful_actor<data> *self, caf::actor logic)
{
    static const std::string name{"client:connected_2 "};

    self->link_to(logic);
    std::cout << "Sending to logic start command" << std::endl;
    self->send(logic, start::value, self);

    return {
        [=](ping) {
            self->request(self->state.server, std::chrono::milliseconds(1000), ping::value, self).then(
                [&](std::string value) {
                    // aout(self) << "[" << name << "] INFO : " << value << endl;
                    self->delayed_send(self, std::chrono::seconds(1), ping::value);
                },
                [=](error &err) {
                    aout(self) << "[" << name << "] ERROR: " << self->system().render(err) << std::endl;
                    self->system().middleman();
                    self->send(logic, stop::value);
                    self->become(connecting(self, logic));
                    self->send(self, start::value); // try reconnecting..
                }
            );
        },
        [=](message_, const std::string &str){
            // aout(self) << "[" << name << "] INFO : passing along to server..: " << str << std::endl;
            self->send(self->state.server, message_::value, str);
        },
        [=](std::string value) {
            // aout(self) << "[" << name << "] INFO : " << value << endl;
            self->send(logic, value);
            if (broadcast/*HACK*/ && value[0] == 'B') {// BROADCAST
                std::cout << "quitting.." << std::endl;
                self->quit(exit_reason::user_shutdown);
            }
        },
    };
}

behavior connected(stateful_actor<data> *self, caf::actor logic)
{
    static const std::string name{"client:connected_1 "};
    self->link_to(logic);
    return {
        [=](ping) {
            self->request(self->state.server, std::chrono::milliseconds(1000), ping::value, self).then(
                [&](std::string value) {
                    // aout(self) << "[" << name << "] INFO : " << value << endl;
                    self->become(connected_2(self, logic));
                    self->delayed_send(self, std::chrono::seconds(1), ping::value);
                },
                [=](error &err) {
                    aout(self) << "[" << name << "] ERROR: " << self->system().render(err) << std::endl;
                    self->system().middleman();
                    self->become(connecting(self, logic));
                    self->send(self, start::value); // try reconnecting..
                }
            );
        },
        [=](message_, const std::string &str){
            self->delayed_send(self, std::chrono::seconds(1), message_::value, str); // pass along
        },
    };
}

#include <cstdlib>
behavior connecting(stateful_actor<data> *self, caf::actor logic)
{
    static const std::string name{"client:connecting"};
    self->link_to(logic);
    return {
        [=](start) {
            const std::string host{getenv("HOST")};
            int port = 10000;
            aout(self) << "[" << name << "] INFO  : connecting to " << host << ":" << port << std::endl;

            auto p = self->system().middleman().remote_actor(host, port);
            if (!p) {
                cout << "[" << name << "] ERROR: " << self->system().render(p.error()) << endl;
                self->delayed_send(self, std::chrono::seconds(1), start::value);
                return;
            }
            self->state.server = *p;
            //aout(self) << "becoming based on play_sound value: " << (play_sound ? "true" : " false") << std::endl;
            self->become(connected(self, logic));
            self->delayed_send(self, std::chrono::seconds(0), ping::value);
        },
    };
}


class client_wrapper
{
public:
    caf::actor actor_;
    std::chrono::system_clock::time_point last_received_;

public:
    client_wrapper(caf::actor actor)
        : actor_(actor),
          last_received_(std::chrono::high_resolution_clock::now())
    {
    }

    void reset_time()
    {
        last_received_ = std::chrono::high_resolution_clock::now();
    }

    bool operator ==(const client_wrapper &other) const {
        return actor_ == other.actor_;
    }
    bool operator <(const client_wrapper &other) const {
        return actor_ < other.actor_;
    }
};

std::set<client_wrapper> clients;

behavior server(stateful_actor<server_data> *self) {
    return {
        [=](ping, caf::actor requestor) -> message {
            auto now = std::chrono::high_resolution_clock::now();
            for (std::set<client_wrapper>::iterator it = clients.begin(); it != clients.end(); ) {
                client_wrapper &client = const_cast<client_wrapper &>(*it);
                std::chrono::duration<double, std::milli> idle = now - client.last_received_;
                if (idle.count() > 2000.)
                    it = clients.erase(it);
                else {
                    if (client.actor_ == requestor) {
                        client.reset_time();
                    }
                    ++it;
                }
            }
            clients.insert(client_wrapper{requestor});

            //for (auto &client : clients) {
            //    self->send(client.actor_, "HEARTBEAT " + std::to_string(clients.size()));
            //}

            return make_message("Ping? Pong!");
        },
        [=](message_, const std::string &str){
            //std::cout << "Got something..: " << str << " passing along to : " << clients.size() << std::endl;
            for (auto &client : clients) {
                self->send(client.actor_, "BROADCAST: " + str);
            }
        }
    };
}
int main(int argc, char *argv[])
{
    /*
    sf::SoundBuffer buffer;
    //if (!buffer.loadFromFile("alarm.wav"))
    if (!buffer.loadFromFile("Welkom Ray.wav"))
        return -1;
        */

    if (argc == 1) return 1;
    bool is_server = std::string(argv[1]) == "server";
    bool is_client = std::string(argv[1]) == "client";
    bool play_sound = false;
    bool sendmsg = false;

    std::vector<std::string> msgs;
    if (argc == 3) {
        if (std::string(argv[2]) == "sound") {
            // sound broadcaster
            play_sound = true;
        }
        else if (std::string(argv[2]) == "sendmsg") {
            // sound broadcaster
            sendmsg = true;
            for (std::string line; std::getline(std::cin, line);) {
                msgs.push_back(line);
                break; // test
            }
        }
    }

    std::cout << "server: " << std::boolalpha << is_server << std::endl;

    actor_system_config cfg;
    cfg.load<io::middleman>();
    actor_system system(cfg);
   
    if (is_server) {
        auto dr = system.spawn(server);

        int port = 10000;
        auto p = system.middleman().publish(dr, port, nullptr, true);
        if (!p) {
            std::cout << "publishing " << " FAILED..: " << system.render(p.error()) << endl;
            return 1;
        }
        else if (*p != port) {
            std::cout << "publishing " << " FAILED.." << endl;
            return 1;
        }
    }
    else if (is_client) {
        broadcast = sendmsg;

#ifndef NO_AUDIO
        auto logic = (play_sound) ? system.spawn(sound_player, play_sound, sendmsg)
            : system.spawn(console_writer);
#else
        auto logic = system.spawn(console_writer);
#endif
        auto mfa = system.spawn(connecting, logic);

        scoped_actor s(system);
        s->send(mfa, start::value);
        for (auto msg : msgs) {
            s->send(mfa, message_::value, msg);
        }
        s->await_all_other_actors_done();
    }
//    else if (is_sendmsg) {
//        auto mfa = system.spawn(connecting);
//
//        scoped_actor s(system);
//        s->send(mfa, start::value);
//        s->await_all_other_actors_done();
//    }
}

