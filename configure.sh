#!/bin/bash

git submodule update --init --recursive
pushd libs/caf

./configure \
    --build-static-only \
    --no-examples
    --no-qt-examples \
    --no-protobuf-examples \
    --no-curl-examples \
    --no-unit-tests \
    --no-opencl \
    --no-benchmarks \
    --no-tools \
    --no-python

make -j 1 # $(nproc)
sudo make install

